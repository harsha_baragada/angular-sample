import { AppService } from './../card/service/app.service';
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnChanges {
  posts: any[] = [];
  constructor(private service: AppService) {
    console.log('This is a constructor');
  }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    console.log('This is ngOnInIt');
    this.service.getPosts().subscribe((response) => {
      console.log(response);
      this.posts = response;
    });
  }
  likeFromChild(event) {
    alert(event);
  }
}
