import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() post: any;
  @Output() like = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  liked() {
    this.like.emit('you liked ' + this.post.id);
  }
}
